package com.app.utils;

public class AppConstants {
	
	//Configurations
	public static final String FILE_JSON = "res/banking.json";
	
	//Greetings
	public static final String MSG_WELCOME = "Hello! Bonjour! \nWelcome to All-In-One-Banking.";
	public static final String MSG_GOODBYE = "Thanks! Have a nice day. See you soon!";
	
	//Errors
	public static final String INVALID_INPUT = "Invalid Input! Please recheck and try again.";
	public static final String NO_RESULTS = "No Result found! Please recheck and try again.";
	public static final String NEG_AMOUNT = "Invalid Input! Please make sure Amount is Greater than 0.";
	public static final String INSUFFICIENT_AMOUNT = "Bank Account doesn't have enough Money to Perform this Withdrawl.";
	public static final String NOT_ENOUGH_BALANCE = "Error! Please make sure that the Sender has enough Money to send.";
	public static final String CANT_OPEN_IRA = "Can't Open Retirement Account as User should be of 60 years or older.";
	
}