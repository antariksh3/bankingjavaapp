package com.app.models;

public class UserDetails {
	
	private String firstName ;
	private String lastName ;
	private int bankAccountNumber ;
	private int sinNumber ;
	private String address ;
	private String email ;
	private int phoneNumber ;
	private double balance ;
	private int age ;
	private String accountType ;
	
	public UserDetails(String firstName, String lastName, int bankAccountNumber, int sinNumber, String address,
			String email, int phoneNumber, double balance, int age, String accountType) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.bankAccountNumber = bankAccountNumber;
		this.sinNumber = sinNumber;
		this.address = address;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.balance = balance;
		this.age = age;
		this.accountType = accountType;
	}
	
	@Override
	public String toString() {
		return "UserDetails [firstName=" + firstName + ", lastName=" + lastName + ", bankAccountNumber="
				+ bankAccountNumber + ", sinNumber=" + sinNumber + ", address=" + address + ", email=" + email
				+ ", phoneNumber=" + phoneNumber + ", balance=" + balance + ", age=" + age + ", accountType="
				+ accountType + "]";
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getBankAccountNumber() {
		return bankAccountNumber;
	}
	public void setBankAccountNumber(int bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	public int getSinNumber() {
		return sinNumber;
	}
	public void setSinNumber(int sinNumber) {
		this.sinNumber = sinNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	
}
