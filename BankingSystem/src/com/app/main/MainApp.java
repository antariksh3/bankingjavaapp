package com.app.main;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Scanner;

import javax.swing.text.Utilities;

import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintStream;

import com.google.gson.JsonParser;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;

import java.util.Map.Entry;
import com.app.models.UserDetails;
import com.app.utils.AppConstants;

public class MainApp {
	
	//Declarations
	static ArrayList<UserDetails> userDetailsList ;
	static Scanner scanner ;
	static boolean terminate = false ;
	static boolean greetingShow = false ;
	
	public static void main (String[] args) {

		//Initializations
		userDetailsList = getUserDetailsList();
		scanner = new Scanner(System.in);
		int choice = 1 ; //choice is set to 1 so that it enter the while loop
		
		//Initial Methods
		loadUserDetailsFromJSON();
		
		//Welcome Greetings
		if(!greetingShow) {
			println(AppConstants.MSG_WELCOME);
			greetingShow = true;
		}
		
		
		//Menu Options
		while (choice > 0 && choice < 12 && !terminate ) {	
			displayMenu();
			try {
				choice = scanner.nextInt();		
				switch (choice) {
				case 1:
					searchAndDisplayAccountBalance();
					break;
				case 2:
					depositMoneyToBankAccount();
					break;
				case 3:
					withdrawMoneyFromBankAccount();
					break;
				case 4:
					trasnferMoney();
					break;
				case 5:
					payUtilityBills();
					break;
				case 6:
					payCreditCardBill();
					break;
				case 7:
					createNewBankAccount();
					break;
				case 8:
					displayAllUsers();
					break;
				case 9:
					saveDataToJSON();
					break;
				case 10:
					closeBankAccount();
					break;
				case 11:
					updateUserDetails();
					break;
				default:
					sureToExit();
					break;
				}
			} catch (Exception e) {
				if(e instanceof InputMismatchException) {
					println(AppConstants.INVALID_INPUT);
					terminate = true;
				}
			}
		}
	}

	private static void displayMenu() {
		println("");
		println("Enter your choice");
		println("1.  Display User Balance");
		println("2.  Deposit Money");
		println("3.  Withdraw Money");
		println("4.  Transfer Money");
		println("5.  Pay Utility Bills");
		println("6.  Pay Credit Card Bill");
		println("7.  Create New Bank Account");
		println("8.  Display Details of All Users' Bank Accounts");
		println("9.  Save All Changes");
		println("10. Close Bank Account");
		println("11. Update User Details");
		print("Enter Any Other Key to Exit: ");
	}

	private static void searchAndDisplayAccountBalance() {
		clear();
		print("Enter the Bank Account Number for which you want to view balance: ");
		int input = 0;
		input = scanner.nextInt();
		
		boolean found = false ;
		for (UserDetails user : userDetailsList) {
			if (user.getBankAccountNumber() == input) {
				println("Balance in "+user.getFirstName() + " " + user.getLastName() + " 's Account Number "
						+ user.getBankAccountNumber() + " (" + user.getAccountType() + ") is: "+ user.getBalance());
				println("");
				found = true ;
			}
		}
		if(found == true) {
			return ;
		}
		if(found == false && !terminate) {
			println(AppConstants.NO_RESULTS);
			println("");
		}
	}
	
	private static void displayAccountBalance(int accNo) {
		boolean found = false ;
		for (UserDetails user : userDetailsList) {
			if (user.getBankAccountNumber() == accNo) {
				println("Balance in "+user.getFirstName() + " " + user.getLastName() + " 's Account Number "
						+ user.getBankAccountNumber() + " (" + user.getAccountType() + ") is: "+ user.getBalance());
				found = true ;
			}
		}
		if(found == false && !terminate) {
			println(AppConstants.NO_RESULTS);
		}
	}
	
	private static void loadUserDetailsFromJSON () {
		JsonParser parser = new JsonParser();
		try {
			JsonObject jsonObj = (JsonObject) parser.parse(new FileReader(AppConstants.FILE_JSON));
			JsonArray jsonArray = jsonObj.getAsJsonArray("users");
			
	        for (int i = 0; i < jsonArray.size() ; i++) {
	        	String first_name = jsonArray.get(i).getAsJsonObject().get("firstName").getAsString();
	            String last_name = jsonArray.get(i).getAsJsonObject().get("lastName").getAsString();
	            int bank_account_number = jsonArray.get(i).getAsJsonObject().get("bankAccountNumber").getAsInt();
	            int sin = jsonArray.get(i).getAsJsonObject().get("sinNumber").getAsInt();
	            String address = jsonArray.get(i).getAsJsonObject().get("address").getAsString();
	            String email = jsonArray.get(i).getAsJsonObject().get("email").getAsString();
	            int phone = jsonArray.get(i).getAsJsonObject().get("phoneNumber").getAsInt();
	            double balance = jsonArray.get(i).getAsJsonObject().get("balance").getAsDouble();
	            int age = jsonArray.get(i).getAsJsonObject().get("age").getAsInt();
	            String account_type = jsonArray.get(i).getAsJsonObject().get("accountType").getAsString();
	            UserDetails userDetails = new UserDetails(first_name, last_name, bank_account_number, sin, address, email, phone, balance, age, account_type);
	            userDetailsList.add(userDetails);	            
	        }
		} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
			print("Parsing Exception Occured!");
			e.printStackTrace();
		}
	}
	
	private static void depositMoneyToBankAccount() {
		clear();
		int input = 0;
		double amount = 0;
		print("Enter Bank Account Number: ");
		input = scanner.nextInt();
		
		UserDetails currentUser = getUserByAccountNumber(input);
		if(currentUser == null){
			println("");
			terminate = true;
		} else {
			print("Enter Amount you want to Add to Bank Account Number "+input+": ");
			amount = scanner.nextDouble();
		}
		if(amount > 0.00) {
			double sum = currentUser.getBalance() + amount;
			currentUser.setBalance(sum);
			println("Updated Balance for Account Number "+ currentUser.getBankAccountNumber() + ": " +currentUser.getBalance());
		}else {
			terminate = true;
			print(AppConstants.NEG_AMOUNT);
		}		
		println("");
	}
	
	private static void depositMoneyToBankAccount(int input,double amount) {
		clear();
		UserDetails currentUser = getUserByAccountNumber(input);
		if(currentUser == null){
			print(AppConstants.NO_RESULTS);
			return ;
		} 
		if(amount > 0.00) {
			double sum = currentUser.getBalance() + amount;
			currentUser.setBalance(sum);
			println("Updated Balance for Account Number "+ currentUser.getBankAccountNumber() + ": " +currentUser.getBalance());
		} else {
			print(AppConstants.NEG_AMOUNT);
		}		
	}
	
	private static void withdrawMoneyFromBankAccount() {
		clear();
		int input = 0;
		double amount = 0;
		print("Enter Bank Account Number: ");
		input = scanner.nextInt();
		UserDetails currentUser = getUserByAccountNumber(input);
		if ( currentUser == null ) {
			print(AppConstants.NO_RESULTS);
		} else {
			print("Enter Amount you want to Withdraw From Bank Account Number "+input+": ");
			amount = scanner.nextDouble();
		}
		if(amount > 0.00 && currentUser.getBalance() >= amount) {
			double sum = currentUser.getBalance() - amount;
			currentUser.setBalance(sum);
		}else {
			terminate = true;
			print(AppConstants.INSUFFICIENT_AMOUNT);
		}		
		println("");
	}
	
	private static void withdrawMoneyFromBankAccount(int input, double amount) {
		clear();
		UserDetails currentUser = getUserByAccountNumber(input);
		if(currentUser == null){
			print(AppConstants.NO_RESULTS);
			return ;
		} 
		if (amount <= 0.00) {
			print(AppConstants.NEG_AMOUNT);
			return ;
		}
		if (currentUser.getBalance() >= amount) {
			double sum = currentUser.getBalance() - amount;
			currentUser.setBalance(sum);
		}else {
			print(AppConstants.INSUFFICIENT_AMOUNT);
		}		
	}
	
	private static void payUtilityBills() {
		clear();		
		print("Enter SIN Numbers to fetch Utility Bills: ");
		int sin = scanner.nextInt();
		UserDetails currentUser = getUserBySinNumber(sin);
		
		if(currentUser == null){
			println("No user found for SIN Number: "+sin);
		} else {
			if (com.app.utils.Utilities.get50PercentOdds()){
				int utBill = com.app.utils.Utilities.generateUtilityBill();
				Double bill = Double.parseDouble(String.valueOf(utBill));
				println("Your Pending Payment for Utility Bill is: "+bill);
				if(currentUser.getAccountType().equalsIgnoreCase("IRA")){
					println("All IRA Account Holders get a 20% Senior Citizen Discount on Utility Bills");
					bill = bill * 0.8;
					println("Your Pending Payment for Utility Bill (After Senior Citizen Discount) is: "+bill);
				}
				print("Would you like to pay right now? (Enter y or n) : ");
				String yn = scanner.next();
				if(yn.equalsIgnoreCase("y")){
					withdrawMoneyFromBankAccount(currentUser.getBankAccountNumber(), bill);
					displayAccountBalance(currentUser.getBankAccountNumber());
				} else {
					println("Alright! Be sure to pay before the due date to avoid late fees.");
				}
			} else {
				println("You do not have any Utility Bills Currently. Please check later.");
			}
		}	
		println("");
	}
	
	private static void payCreditCardBill() {
		clear();
		print("Enter SIN Numbers to fetch Credit Card Bill: ");
		int sin = scanner.nextInt();
		UserDetails currentUser = getUserBySinNumber(sin);
		
		if(currentUser == null){
			println("No user found for SIN Number: "+sin);
		} else {
			if (com.app.utils.Utilities.get50PercentOdds()){
				int bill = com.app.utils.Utilities.generateUtilityBill();
				println("Your Pending Payment for Credit Card Bill is: "+bill);
				print("Would you like to pay right now? (Enter y or n) : ");
				String yn = scanner.next();
				if(yn.equalsIgnoreCase("y")){
					withdrawMoneyFromBankAccount(currentUser.getBankAccountNumber(), bill);
					displayAccountBalance(currentUser.getBankAccountNumber());
				} else {
					println("Alright! Be sure to pay before the due date to avoid late fees.");
				}
				
			} else {
				println("You do not have any Credit Card Bill Currently. Please check later.");
			}
		}
		println("");
	}
	
	private static void trasnferMoney() {
		clear();
		println("Enter Sender's Bank Account Number: ");
		int bankAccount1 = scanner.nextInt();
		if(!isBankAccountValid(bankAccount1)) {
			println(AppConstants.NO_RESULTS);
			println("");
			return ;
		}
		println("Enter Reciever's Bank Account Number: ");
		int bankAccount2 = scanner.nextInt();
		if(!isBankAccountValid(bankAccount2)) {
			println(AppConstants.NO_RESULTS);
			println("");
			return ;
		}
		displayAccountBalance(bankAccount1);
		displayAccountBalance(bankAccount2);
		println("Enter Amount you want to transfer from "+getUserByAccountNumber(bankAccount1).getFirstName() + " " 
				+ getUserByAccountNumber(bankAccount1).getLastName() +" to " + getUserByAccountNumber(bankAccount2).getFirstName() + " " 
				+ getUserByAccountNumber(bankAccount2).getLastName() + " : " );
		int amount = scanner.nextInt();
		withdrawMoneyFromBankAccount(bankAccount1, amount);
		depositMoneyToBankAccount(bankAccount2, amount);
		println("Transfer Succesfull!");
		displayAccountBalance(bankAccount1);
		displayAccountBalance(bankAccount2);
		println("");
	}
	
	private static void createNewBankAccount() {
		clear();
		Scanner scanner2 = new Scanner(System.in);
		println("-----New Bank Account Form-----");
		print("Enter User's First Name: ");
		String fname = scanner.next();
		print("Enter User's Last Name: ");
		String lname = scanner.next();
		print("Enter User's SIN Number: ");
		int sin = scanner.nextInt();
		print("Enter User's Full Address: ");
		String addr = scanner2.nextLine();
		print("Enter User's Email Address: ");
		String email = scanner.next();
		print("Enter User's Phone Number: ");
		int phoneNum = scanner.nextInt();
		print("Enter User's Age: ");
		int age = scanner.nextInt();
		
		println("");
		println("Which type of Account would you like to Open?");
		println("1. Savings");
		println("2. Checking");
		println("3. IRA ( Note - Age Should be greater than 60 )");
		print("Enter Choice: ");
		
		int choice = scanner.nextInt();
		String accountType = "";
		double initAmount = 0 ;
		switch(choice) {
			case 1:
				print("Enter Initial Amount for Savings Account: ");
				initAmount = scanner.nextDouble();
				createNewAccount(fname,lname,sin,addr,email,phoneNum,initAmount,age,"Savings");
				break;
			case 2:
				createNewAccount(fname,lname,sin,addr,email,phoneNum,initAmount,age,"Checking");
				break;
			case 3:
				if ( age < 60 ) {
					clear();
					println(AppConstants.CANT_OPEN_IRA);
				} else {
					createNewAccount(fname,lname,sin,addr,email,phoneNum,initAmount,age,"IRA");
				}
				break;
			default:
				println(AppConstants.INVALID_INPUT);
				break;
		}
		println("");
	}

	private static void createNewAccount(String fname, String lname, int sin, String addr, String email,
			int phoneNum, double initAmount, int age, String accountType) {
		int newBankAccNumber = 0 ;
		do {
			newBankAccNumber = com.app.utils.Utilities.generateNewBankAccountNumber();
		} while (isBankAccountValid(newBankAccNumber));
		UserDetails newUser = new UserDetails(fname, lname, newBankAccNumber, sin, addr, email, phoneNum, initAmount, age, accountType);
		userDetailsList.add(newUser);
		println("Account Opened! Current Balance: "+initAmount);
	}
	
	private static void closeBankAccount() {
		clear();
		print("Enter the Bank Account Number that you want to close down : ");
		int input = scanner.nextInt();
		UserDetails currentUser = getUserByAccountNumber(input);
		if(currentUser==null){
			println(AppConstants.NO_RESULTS);
			println("");
			return ;
		}
		println("Are you sure you want to close "+currentUser.getFirstName() + " " + currentUser.getLastName() + " 's Account Number "+
				currentUser.getBankAccountNumber() + " ( " + currentUser.getAccountType() + " ) ");
		if(currentUser.getBalance() > 0.0) {
			println("Your Balance of "+ currentUser.getBalance() + " will be sent to your Address "+ currentUser.getAddress() + " via Check" );
		}
		print("Enter y or n : ");
		String choice = scanner.next();
		if(choice.equalsIgnoreCase("y")){
			userDetailsList.remove(currentUser);
			println("Account Deleted! "+currentUser.toString());
		} 
		println("");
		return ;
	}
	
	private static void saveDataToJSON() {
		clear();
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("users", userDetailsList);
		print("Saving JSON: " + jsonObj.toString());
		try (PrintStream out = new PrintStream(new FileOutputStream(AppConstants.FILE_JSON))) {
		    out.print(jsonObj.toString());
		    println("JSON Saved!");
		    println("");
		} catch (Exception e) {
			println("Exception while trying to Parse Data into JSON.");
			e.printStackTrace();
		}
		println("");
	}
	
	private static void updateUserDetails() {
		clear();
		print("Enter Bank Account whose User Details Need to be Updated: ");
		int accNum = scanner.nextInt();
		
		UserDetails currentUser = getUserByAccountNumber(accNum);
		Scanner scanner2 = new Scanner(System.in);
		
		if(currentUser == null) {
			println(AppConstants.NO_RESULTS);
			println("");
			return ;
		}
		
		println("");
		println("Current User Details: "+currentUser.toString());
		println("");
		println("Please Select What User Detail you want to Edit for Bank Account Number "+accNum);
		println("1.  Update First Name "+ "( Current Value : " + currentUser.getFirstName() + " )");
		println("2.  Update Last Name "+ "( Current Value : " + currentUser.getLastName() + " )");
		println("3.  Update SIN Number "+ "( Current Value : " + currentUser.getSinNumber() + " )");
		println("4.  Update Address "+ "( Current Value : " + currentUser.getAddress() + " )");
		println("5.  Update Email Address "+ "( Current Value : " + currentUser.getEmail() + " )");
		println("6.  Update Phone Number "+ "( Current Value : " + currentUser.getPhoneNumber() + " )");
		println("7.  Update Age "+ "( Current Value : " + currentUser.getAge() + " )");
		print("Enter the Choice you want to Enter: ");
		
		int choice = scanner.nextInt();
		println("");
		
		UserDetails oldUser = currentUser ;
		
		switch(choice) {	
			case 1:
				print("Enter First Name: ");
				String fname = scanner.next();
				currentUser.setFirstName(fname);
				updateUserDetailsBasedOnAccountNumber(oldUser, currentUser, accNum);
				break;
			case 2:
				print("Enter Last Name: ");
				String lname = scanner.next();
				currentUser.setLastName(lname);;
				updateUserDetailsBasedOnAccountNumber(oldUser, currentUser, accNum);
				break;
			case 3:
				print("Enter SIN Number: ");
				int sin = scanner.nextInt();
				currentUser.setSinNumber(sin);
				updateUserDetailsBasedOnAccountNumber(oldUser, currentUser, accNum);
				break;
			case 4:
				print("Enter Updated Address: ");
				String newAddress = scanner2.nextLine();
				currentUser.setAddress(newAddress);
				updateUserDetailsBasedOnAccountNumber(oldUser, currentUser, accNum);
				break;
			case 5:
				print("Enter Email Address: ");
				String newEmailAddress = scanner.next();
				currentUser.setEmail(newEmailAddress);;
				updateUserDetailsBasedOnAccountNumber(oldUser, currentUser, accNum);
				break;
			case 6:
				print("Enter Phone Number: ");
				int newPhone = scanner.nextInt();
				currentUser.setPhoneNumber(newPhone);;
				updateUserDetailsBasedOnAccountNumber(oldUser, currentUser, accNum);
				break;
			case 7:
				print("Enter Age: ");
				int newAge = scanner.nextInt();
				currentUser.setAge(newAge);;
				updateUserDetailsBasedOnAccountNumber(oldUser, currentUser, accNum);
				break;
			default:
				println(AppConstants.INVALID_INPUT);
				println("");
				break;
		}
		
	}
	
	private static void updateUserDetailsBasedOnAccountNumber(UserDetails oldUser, UserDetails newUser, int accNum) {
		println("");
		int index = userDetailsList.indexOf(oldUser);
		userDetailsList.remove(index);
		userDetailsList.add(index,newUser);
		println("User Details Updated for Account Number: " + accNum);
		println("Updated User: "+ newUser.toString());
		println("");
	}
	


	private static void displayAllUsers() {
		clear();
		if(userDetailsList.size() < 1 ){
			print("No Account Found! Please create some.");
			println("");
			return ;
		}
		for (UserDetails user : userDetailsList) {
			println(user.toString());
		}
		println("");
	}

	private static void sureToExit() {
		clear();
		println("Are you sure you want to Exit?");
		print("Press 'x' to Exit or Any Other Key to Continue: ");
		String choice = scanner.next();
		if(choice.equalsIgnoreCase("x")){
			print(AppConstants.MSG_GOODBYE);
		} else {
			clear();
			main(null);
		}
		
	}

	private static void print(String s) {
		System.out.print(s);
	}
	private static void println(String s) {
		System.out.println(s);
	}
	private static void clear () {
		for(int i=0 ; i<70 ; i++) {
			System.out.print("\n");
		}
	}
	private static void printLine() {
		System.out.print("----------------------------------------------------------------------------------------------------");
	}
	private static ArrayList<UserDetails> getUserDetailsList() {
		//Singleton approach in order to improve optimization
		if(userDetailsList == null) {
			userDetailsList = new ArrayList<>();
		}
		return userDetailsList ;
	}
	
	private static UserDetails getUserByAccountNumber (int account_number) {
		for (UserDetails user : userDetailsList) {
			if (user.getBankAccountNumber() == account_number) {
				return user;
			}
		}
		return null;
	}
	private static UserDetails getUserBySinNumber (int sin) {
		for (UserDetails user : userDetailsList) {
			if (user.getSinNumber() == sin) {
				return user;
			}
		}
		return null;
	}
	private static boolean isBankAccountValid(int backAccount){
		for(UserDetails user : userDetailsList) {
			if(backAccount == user.getBankAccountNumber()) {
				return true;
			}
		}
		return false;
	}
	
	
}
